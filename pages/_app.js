import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Header from "../components/Header"
import { wrapper } from './../store/store'
function MyApp({ Component, pageProps }) {
    return <> 
      
            <Header></Header>
            <Component {...pageProps} />
       
    </>
}

export default wrapper.withRedux(MyApp)
  