import { takeLatest, takeEvery } from "redux-saga/effects";
import * as types from "./../types"
import { getPosts, updatePost, deletePost } from "./postSaga";

export default function* rootSaga() {
    yield takeLatest(types.GET_POSTS, getPosts)
    yield takeLatest(types.UPDATE_POSTS, updatePost)
    yield takeLatest(types.DELETE_POSTS, deletePost)

}