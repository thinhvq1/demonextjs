import { takeLatest, call, put, all } from "redux-saga/effects";
import * as services from "./../../service/postServive"
import * as actions from "./../actions"

export function* getPosts({ params }) {
    try {
        const res = yield fetch('https://jsonplaceholder.typicode.com/posts')
        const data = yield res.json()
        yield put(actions.getPostsSuccess(data.splice(0, 10)));
    } catch (error) {
        yield put(actions.getPostsFail(error.message));
    }
}

export function* updatePost() {

}

export function* deletePost() {

}