import * as types from "../types";

export const fetchPosts = () => async dispatch => {
    dispatch({
        type: types.GET_POST,
        payload: ["1st", "2st", "3st"]
    })
}

export const getPosts = (params = {}) => {
    return {
        type: types.GET_POSTS,
        payload: {
            params
        }
    }
}
export const getPostsSuccess = (data) => {
    return {
        type: types.GET_POSTS_SUCCESS,
        payload: {
            data
        }
    }
}
export const getPostsFail = (error) => {
    return {
        type: types.GET_POSTS_FAIL,
        payload: {
            error
        }
    }
}