import { combineReducers } from "redux";
import thunkReducer from "./thunk";
import postReducer from "./postReducer"
export default combineReducers({
    thunk: thunkReducer,
    posts: postReducer
})