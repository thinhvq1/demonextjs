import * as types from "../types";

const initialState = {
    posts: null,
    post: {},
    error: null,
}

export const thunkReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_POST:
            return {
                ...state,
            }

        case types.GET_POSTS_SUCCESS:
            return {
                ...state,
                posts: action.payload.data
            }
        case types.GET_POSTS_FAIL:
            return {
                ...state,
                error: action.payload.error
            }
        default:
            return {
                ...state
            }
    }
}

export default thunkReducer;